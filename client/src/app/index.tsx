import * as React from 'react';
import './styles';

import { Route, Switch } from 'react-router';
import { App as TodoApp } from 'app/containers/App';
import { hot } from 'react-hot-loader';
import { Preconditions } from 'app/containers/Preconditions';
import { Game } from 'app/containers/Game';

export const App = hot(module)(() => (
  <Switch>
    <Route path="/" exact component={TodoApp} />
    <Route path="/preconditions" component={Preconditions} />
    <Route path="/game" component={Game} />
  </Switch>
));
