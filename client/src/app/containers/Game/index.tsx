import * as React from 'react';
const style = require('./style.scss');

import { Field } from 'app/components/Field';
import { connect } from 'react-redux';
import { RootState } from 'app/reducers';
import { bindActionCreators, Dispatch } from 'redux';
import { omit } from 'app/utils';
import {EnemyPlayerFieldActions} from "app/actions/enemyPlayerField";
import {GameActions} from 'app/actions/game';

export namespace Game {
  export interface Props {
    currentPlayerField: RootState.CurrentPlayerFieldState;
    enemyPlayerField: RootState.EnemyPlayerFieldState;
    actionsEnemyField: EnemyPlayerFieldActions;
    actionsGame: GameActions;
  }
}

@connect(
  (state: RootState): Pick<Game.Props, 'currentPlayerField' | 'enemyPlayerField'> => ({
    currentPlayerField: state.currentPlayerField,
    enemyPlayerField: state.enemyPlayerField
  }),
  (dispatch: Dispatch): Pick<Game.Props, 'actionsEnemyField' | 'actionsGame'> => ({
    actionsEnemyField: bindActionCreators(omit(EnemyPlayerFieldActions, 'Type'), dispatch),
    actionsGame: bindActionCreators(omit(GameActions, 'Type'), dispatch),
  })
)
export class Game extends React.Component<Game.Props> {

  componentDidMount = () => {
    this.props.actionsGame.initGame();
  };

  handleEnemyFieldAttack = (cellNumber: string) => {
    this.props.actionsEnemyField.attackEnemyPlayerField(cellNumber);
  };

  render() {
    const { currentPlayerField, enemyPlayerField } = this.props;
    return (
      <div className={style.gamePage}>
        <header>
          header
        </header>
        <div className={style.gameSections}>
          <div className={style.playerSide}>
            <Field
              size={'90vh'}
              currentPlayerField={currentPlayerField.field}
            />
          </div>
          <div className={style.enemySide}>
            <Field
              size={'90vh'}
              onFieldCellSelect={this.handleEnemyFieldAttack}
              currentPlayerField={enemyPlayerField.field}
            />
          </div>
        </div>
      </div>
    );
  }
}
