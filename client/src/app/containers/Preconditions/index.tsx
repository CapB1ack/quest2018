import * as React from 'react';
const style = require('./style.scss');

import {Field} from 'app/components/Field';
import {connect} from 'react-redux';
import {RootState} from 'app/reducers';
import {bindActionCreators, Dispatch} from 'redux';
import {omit} from 'app/utils';
import {CurrentPlayerFieldActions} from 'app/actions';
import {ShipsTracker} from "app/components/ShipsTracker";
import { RouteComponentProps } from 'react-router';

export namespace Preconditions {
  export interface Props extends RouteComponentProps<void>{
    currentPlayerField: RootState.CurrentPlayerFieldState;
    actions: CurrentPlayerFieldActions;
  }
}

@connect(
  (state: RootState): Pick<Preconditions.Props, 'currentPlayerField'> => ({
    currentPlayerField: state.currentPlayerField
  }),
  (dispatch: Dispatch): Pick<Preconditions.Props, 'actions'> => ({
    actions: bindActionCreators(omit(CurrentPlayerFieldActions, 'Type'), dispatch)
  })
)
export class Preconditions extends React.Component<Preconditions.Props> {

  handleClear = () => {
    this.props.actions.clearField();
  };

  handleStartGame = () => {
    this.props.history.push(`/game`);
  };

  render() {
    const {currentPlayerField: { field, ships }, actions: {markCellAsShip}} = this.props;
    return (
      <div className={style.page}>
        <Field
          currentPlayerField={field}
          onFieldCellSelect={markCellAsShip}
        />
        <div className={style.side}>
          <div className={style.username}>Команда: Гуси, Игрок: Семён</div>
          <div className={style.sections}>
            <div className={style.ships}>
              <button onClick={this.handleClear}>очистить</button>
              <ShipsTracker ships={ships} osStartGame={this.handleStartGame}/>
            </div>
            <div className={style.rules}>rules</div>
          </div>
        </div>
      </div>
    );
  }
}
