import {ICellModel, IFieldModel} from "app/models/FieldModel";

export const parseToPoints = (str: any) => str.split('-').map((e: any) => +e);
export const parseToString = (point: [number, number]) => `${point[0]}-${point[1]}`;
export const FOUR_AVAILABLE_SIDES = [[0, -1], [-1, 0], [1, 0], [0, 1]];
export const FOUR_FORBIDDEN_SIDES = [[-1, -1], [-1, 1], [1, 1], [1, -1]];

export const findAvailablePaths = (SIZE: number, position: [number, number], side: number[][]) => {
  return side.reduce((acc: number[][], side) => {
    const dx = position[0] + side[0];
    const dy = position[1] + side[1];
    return [dx, dy].every(onePoint => 1 <= onePoint && onePoint < SIZE) ? [...acc, [dx, dy]] : acc;
  }, []);
};

export const markAllAsInvalid = (cells: string[][], state: IFieldModel, minLength: number) => {
  cells.length >= minLength && cells.reduce((acc, cur) => [...acc, ...cur], []).map((index: string) => {
    state[index] = {
      ...state[index],
      isInvalidCell: true
    };
  });
};

export const markAllAsUnavailable = (cells: string[][], state: IFieldModel) => {
  cells.reduce((acc, cur) => [...acc, ...cur], []).map(index => {
    findAvailablePaths(11, parseToPoints(index), FOUR_AVAILABLE_SIDES).map(availPoint => {
      const [x, y] = availPoint;
      const key = `${x}-${y}`;
      state[key] = {
        ...state[key],
        isUnavailableCell: !state[key].isShipCell
      }
    });
  });
};

export const validateUnavailableCells = (state: IFieldModel) => {
  // проверка на появление досупных полей
  Object.keys(state).map((cellNumber: string) => {
    if ((state[cellNumber] as ICellModel).isUnavailableCell) {
      const shouldActivateCell = findAvailablePaths(11, parseToPoints(cellNumber), [...FOUR_FORBIDDEN_SIDES, ...FOUR_AVAILABLE_SIDES])
        .every(availPoint => {
          const [x, y] = availPoint;
          const key = `${x}-${y}`;
          return !state[key].isShipCell;
        });
      if (shouldActivateCell) {
        state[cellNumber] = {
          ...state[cellNumber],
          isUnavailableCell: false
        }
      }
    }
  });
};
