import * as io from 'socket.io-client';

const socket: any = io('ws://localhost:9999', {transports: ['websocket', 'polling', 'flashsocket']});

export default socket;
