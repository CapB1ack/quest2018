import {RootState} from 'app/reducers';
import {
  findAvailablePaths,
  FOUR_AVAILABLE_SIDES,
  FOUR_FORBIDDEN_SIDES,
  markAllAsInvalid,
  markAllAsUnavailable,
  parseToPoints,
  parseToString, validateUnavailableCells
} from 'app/utils/core';

export const validateField = (oldState: RootState.CurrentPlayerFieldState, point: string) => {
  const currentPoint = parseToPoints(point);
  const field = {
    ...oldState.field,
    [point]: {isShipCell: !oldState.field[point].isShipCell}
  };

  //1 - проход по всем кораблям и поиск заблокированных точек
  Object.keys(field).map((cellNumber: string) => {
    // обнуляю флаги валидации.
    field[cellNumber] = {
      ...field[cellNumber],
      isInvalidCell: false
    };

    field[cellNumber].isShipCell &&
    findAvailablePaths(11, currentPoint, FOUR_FORBIDDEN_SIDES)
      .map((availPoint) => {
        // поиск ЗАПРЕЩЕННЫХ клеток по 4-м углам
        const [x, y] = availPoint;
        const key = `${x}-${y}`;
        if (field[key].isShipCell) {
          console.log('корабль в запрещенной клетке: ', key, ' был удален.');
        }
        field[key] = {
          isShipCell: false,
          isUnavailableCell: true
        };
      });
  });

  //2 - пострить группы связанных кораблей
  let shipsGroups: { [index: number]: string[][] } = {1: [], 2: [], 3: [], 4: [], 5: []};
  const visited: string[] = [];
  Object.keys(field).map((cellNumber: any) => {
    if (field[cellNumber].isShipCell && !visited.includes(cellNumber)) {
      const Q = [cellNumber];
      for (let q of Q) {
        const point = parseToPoints(q);
        const nextPoints = findAvailablePaths(11, point, FOUR_AVAILABLE_SIDES);
        nextPoints.map((nextPoint: any) => {
          const keyNextPoint = parseToString(nextPoint);
          if (field[keyNextPoint].isShipCell && !Q.includes(keyNextPoint)) {
            Q.push(keyNextPoint);
          }
        });
      }
      Q.map(el => visited.push(el));
      const len = Math.min(Q.length, 5);
      shipsGroups[len] = [...shipsGroups[len], Q];
    }
  });

  // TODO: добавить в стейт вложенность и перенести туда филды, а добавить еще бранч с количеством

  //3 - валидировать количество групп
  // группы с длинной более 4 в ряд
  markAllAsInvalid(shipsGroups[5], field, 1);

  // 2 и более групп с 4 в ряд
  markAllAsInvalid(shipsGroups[4], field, 2);
  if (shipsGroups[4].length === 1) {
    markAllAsUnavailable(shipsGroups[4], field);
  }
  // 3 и более групп с 3 в ряд
  markAllAsInvalid(shipsGroups[3], field, 3);
  if (shipsGroups[4].length === 1 && shipsGroups[3].length >= 1) {
    markAllAsUnavailable(shipsGroups[3], field);
  }
  // 4 и более групп с 2 в ряд
  markAllAsInvalid(shipsGroups[2], field, 4);
  if (shipsGroups[4].length === 1 && shipsGroups[3].length === 2 && shipsGroups[2].length >= 1) {
    markAllAsUnavailable(shipsGroups[2], field);
  }
  // 5 и более групп с 1
  markAllAsInvalid(shipsGroups[1], field, 5);
  if (shipsGroups[4].length === 1 && shipsGroups[3].length === 2 && shipsGroups[2].length === 3 && shipsGroups[4].length >= 1) {
    markAllAsUnavailable(shipsGroups[1], field);
  }

  validateUnavailableCells(field);

  return {
    ships: {
      1: shipsGroups[1].length,
      2: shipsGroups[2].length,
      3: shipsGroups[3].length,
      4: shipsGroups[4].length,
      5: shipsGroups[5].length
    },
    field
  };
};
