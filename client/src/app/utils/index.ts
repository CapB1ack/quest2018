import {RootState} from 'app/models/stateModel';
import EnemyPlayerFieldState = RootState.EnemyPlayerFieldState;
import CurrentPlayerFieldState = RootState.CurrentPlayerFieldState;

export const SIZE: number[] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
const LETTERS: string[] = ['', 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'К'];

export function omit<T extends object, K extends keyof T>(target: T, ...omitKeys: K[]): Omit<T, K> {
  return (Object.keys(target) as K[]).reduce(
    (res, key) => {
      if (!omitKeys.includes(key)) {
        res[key] = target[key];
      }
      return res;
    },
    {} as any
  );
}

const getValueForCell = (row: number, cell: number) => {
  if (row === 0) {
    return cell === 0 ? void 0 : cell;
  } else if (cell === 0) {
    return LETTERS[row];
  }
  return void 0;
};

export const getInitialFieldState = () => {
  const state: CurrentPlayerFieldState  = {
    field: {},
    ships: {
      1: 0,
      2: 0,
      3: 0,
      4: 0,
      5: 0
    }
  };

  SIZE.map((rowNum) => {
    SIZE.map((cellNum) => {
      const key = `${rowNum}-${cellNum}`;
      state.field[key] = {
        isHiddenCell: rowNum === 0 && cellNum === 0,
        valueOfCell: getValueForCell(rowNum, cellNum)
      }
    });
  });
  return state;
};

export const getInitialEnemyFieldState = () => {
  const state: EnemyPlayerFieldState = {
    field: {}
  };

  SIZE.map((rowNum) => {
    SIZE.map((cellNum) => {
      const key = `${rowNum}-${cellNum}`;
      state.field[key] = {
        isHiddenCell: rowNum === 0 && cellNum === 0,
        valueOfCell: getValueForCell(rowNum, cellNum)
      }
    });
  });
  return state;
};
