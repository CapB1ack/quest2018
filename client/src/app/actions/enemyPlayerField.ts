import { createAction } from 'redux-actions';

export namespace EnemyPlayerFieldActions {
  export enum Type {
    ATTACK_ENEMY_FIELD = 'ATTACK_ENEMY_FIELD',
  }

  export const attackEnemyPlayerField = createAction<string>(Type.ATTACK_ENEMY_FIELD);
}

export type EnemyPlayerFieldActions = Omit<typeof EnemyPlayerFieldActions, 'Type'>;
