import { createAction } from 'redux-actions';

export namespace GameActions {
  export enum Type {
    INIT = 'INIT',
  }

  export const initGame = createAction(Type.INIT);
}

export type GameActions = Omit<typeof GameActions, 'Type'>;
