import { createAction } from 'redux-actions';

export namespace CurrentPlayerFieldActions {
  export enum Type {
    ATTACK_CURRENT_FIELD = 'ATTACK_CURRENT_FIELD',
    MARK_CELL_AS_SHIP = 'MARK_CELL_AS_SHIP',
    CLEAR_FIELD = 'CLEAR_FIELD'
  }

  export const attackCurrentPlayerField = createAction(Type.ATTACK_CURRENT_FIELD);
  export const markCellAsShip = createAction<string>(Type.MARK_CELL_AS_SHIP);
  export const clearField = createAction(Type.CLEAR_FIELD);
}

export type CurrentPlayerFieldActions = Omit<typeof CurrentPlayerFieldActions, 'Type'>;
