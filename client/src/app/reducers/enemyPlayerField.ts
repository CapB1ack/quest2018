import {RootState} from 'app/models/stateModel';
import {handleActions} from 'redux-actions';
import {getInitialEnemyFieldState} from 'app/utils';
import EnemyPlayerFieldState = RootState.EnemyPlayerFieldState;
import {EnemyPlayerFieldActions} from "app/actions/enemyPlayerField";
import socket from 'app/utils/socket';
import {IWebSocketWithClientId} from 'app/models/websocketModel';

const initialState: EnemyPlayerFieldState = getInitialEnemyFieldState();

export const enemyPlayerFieldReducer = handleActions<EnemyPlayerFieldState, any>({
  [EnemyPlayerFieldActions.Type.ATTACK_ENEMY_FIELD]: (oldState, action) => {

    socket.send(JSON.stringify({
      id: (socket as IWebSocketWithClientId).clientId,
      action: "ATTACK_ENEMY_FIELD"
    }));

    return {
      field: {
        ...oldState.field,
        [action.payload]: {
          ...oldState.field[action.payload],
          isAttackedCell: true
        }
      }
    }
  }
}, initialState);
