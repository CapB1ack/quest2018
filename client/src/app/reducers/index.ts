import { combineReducers } from 'redux';
import { RootState } from '../models/stateModel';
import { todoReducer } from './todos';
import { routerReducer, RouterState } from 'react-router-redux';
import {currentPlayerFieldReducer} from 'app/reducers/currentPlayerField';
import {enemyPlayerFieldReducer} from "app/reducers/enemyPlayerField";
import {gameReducer} from 'app/reducers/game';

export { RootState, RouterState };

// NOTE: current type definition of Reducer in 'react-router-redux' and 'redux-actions' module
// doesn't go well with redux@4
export const rootReducer = combineReducers<RootState>({
  todos: todoReducer as any,
  router: routerReducer as any,
  currentPlayerField: currentPlayerFieldReducer as any,
  enemyPlayerField: enemyPlayerFieldReducer as any,
  game: gameReducer as any
});
