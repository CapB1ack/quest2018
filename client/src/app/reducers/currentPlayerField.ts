import {RootState} from 'app/models/stateModel';
import {handleActions} from 'redux-actions';
import {CurrentPlayerFieldActions} from 'app/actions';
import {getInitialFieldState} from 'app/utils';
import {validateField} from 'app/utils/validator';

const initialState: RootState.CurrentPlayerFieldState = getInitialFieldState();

export const currentPlayerFieldReducer = handleActions<RootState.CurrentPlayerFieldState, any>({
  [CurrentPlayerFieldActions.Type.ATTACK_CURRENT_FIELD]: (oldState, action) => ({
    ...oldState,
    [action.payload.cellNumber]: {isAttackedCell: true}
  }),
  [CurrentPlayerFieldActions.Type.MARK_CELL_AS_SHIP]: (oldState, action) => {
    return validateField(oldState, action.payload);
  },
  [CurrentPlayerFieldActions.Type.CLEAR_FIELD]: () => getInitialFieldState()
}, initialState);
