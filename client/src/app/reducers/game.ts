import {handleActions} from 'redux-actions';
import {IGameState} from 'app/models/GameModel';
import {GameActions} from 'app/actions/game';
import socket from 'app/utils/socket';

const initialState: IGameState = {
  isMyTurn: false,
  isWaiting: false,
  isInitialized: false,
};

export const gameReducer = handleActions<IGameState, any>({
  [GameActions.Type.INIT]: (oldState) => {
    socket.json.send({
      id: socket.clientId,
      type: 'INIT'
    });

    return {
      ...oldState,
      isMyTurn: false,
      isWaiting: true,
      isInitialized: true
    };
  }

}, initialState);
