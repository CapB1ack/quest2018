export interface IWebSocketWithClientId extends WebSocket{
  clientId: number;
}
