import { TodoModel } from 'app/models/index';
import { RouterState } from 'react-router-redux';
import { ICurrentPlayerField } from 'app/models/FieldModel';
import {IEnemyPlayerField} from "app/models/EnemyFieldModel";
import {IGameState} from 'app/models/GameModel';

export interface RootState {
  todos: RootState.TodoState;
  router: RouterState;
  currentPlayerField: RootState.CurrentPlayerFieldState;
  enemyPlayerField: RootState.EnemyPlayerFieldState;
  game: RootState.GameState;
}

export namespace RootState {
  export type TodoState = TodoModel[];
  export type CurrentPlayerFieldState = ICurrentPlayerField;
  export type EnemyPlayerFieldState = IEnemyPlayerField;
  export type GameState = IGameState;
}
