import {IFieldModel} from "app/models/FieldModel";

export interface IEnemyPlayerField {
  field: IFieldModel;
}
