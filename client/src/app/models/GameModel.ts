export interface IGameState {
  isMyTurn: boolean;
  isWaiting: boolean;
  isInitialized: boolean;
}
