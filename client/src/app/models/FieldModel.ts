export interface IFieldModel {
  [index: string]: ICellModel;
}

export interface ICellModel {
  valueOfCell?: string | number;
  isShipCell?: boolean;
  isAttackedCell?: boolean;
  isHiddenCell?: boolean;
  isUnavailableCell?: boolean;
  isInvalidCell?: boolean;
}
export interface IShipsModel {
  [size: number]: number;
}

export interface ICurrentPlayerField {
  field: IFieldModel;
  ships: IShipsModel;
}
