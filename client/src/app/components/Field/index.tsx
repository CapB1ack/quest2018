import * as React from 'react';
import {FieldCell} from 'app/components/FieldCell';
import {IFieldModel} from 'app/models/FieldModel';
import {SIZE} from "app/utils";

const style = require('./style.scss');

export namespace Field {
  export interface Props {
    currentPlayerField: IFieldModel;
    size?: string;
    onFieldCellSelect?: (cellNumber: string) => void;
  }
}

export class Field extends React.Component<Field.Props> {

  constructor(props: Field.Props, context?: any) {
    super(props, context);
  }

  handleFieldCellClick = (cellNumber: string) => {
    const {onFieldCellSelect} = this.props;
    onFieldCellSelect && onFieldCellSelect(cellNumber);
  };

  render() {
    const {size = '100vh'} = this.props;

    return <div className={style.field} style={{width: size, height: size}}>
      {SIZE.map(row => {
        return <div key={row} className={style.row}>
          {SIZE.map(cell => {
            const cellNumber = `${row}-${cell}`;
            return (
              <FieldCell
                key={cellNumber}
                cellNumber={cellNumber}
                onCellClick={this.handleFieldCellClick}
                {...this.props.currentPlayerField[cellNumber]}
            />
            );
          })}
        </div>;
      })}
    </div>;
  }
}
