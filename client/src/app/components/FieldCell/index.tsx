import * as React from 'react';
import {ICellModel} from 'app/models/FieldModel';

const style = require('./style.scss');

export namespace FieldCell {
  export interface Props extends ICellModel {
    onCellClick?: (cellNumber: string) => void;
    cellNumber: string;
  }
}

export class FieldCell extends React.Component<FieldCell.Props> {
  constructor(props: FieldCell.Props, context?: any) {
    super(props, context);
  }

  getClassName = (): string => {
    const {isAttackedCell, valueOfCell, isShipCell, isHiddenCell, isUnavailableCell, isInvalidCell} = this.props;
    return [
      style.default,
      isAttackedCell && style.attacked,
      valueOfCell && style.borderCell,
      isShipCell && style.ship,
      isUnavailableCell && style.unavailable,
      isHiddenCell && style.hidden,
      isInvalidCell && style.invalid
    ]
      .filter(e => !!e)
      .join(' ')
  };

  handleClick = () => {
    const {onCellClick, cellNumber, isUnavailableCell, valueOfCell} = this.props;
    !isUnavailableCell && !valueOfCell && onCellClick && onCellClick(cellNumber);
  };

  render() {
    const {valueOfCell} = this.props;
    return (
      <div
        className={this.getClassName()}
        onClick={this.handleClick}
      >
        {valueOfCell}
      </div>
    );
  }
}
