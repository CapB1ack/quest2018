import * as React from 'react';
import {IShipsModel} from "app/models/FieldModel";
const style = require('./style.scss');

export namespace ShipsTracker {
  export interface Props {
    ships: IShipsModel;
    osStartGame: () => void;
  }
}

export class ShipsTracker extends React.Component<ShipsTracker.Props> {

  constructor(props: ShipsTracker.Props, context?: any) {
    super(props, context);
  }

  handleStartGame = () => {
    this.props.osStartGame();
  };

  getStyles = (type: string) => {
    const {ships} = this.props;
    const map: {[index :string]: number} = {
      'one': 1,
      'two': 2,
      'three': 3,
      'four': 4
    };
    return style[`${type}_${ships[map[type]]}`];
  };

  render() {
    const {ships} = this.props;
    const isValid = ships[1] === 4 &&
      ships[2] === 3 &&
      ships[3] === 2 &&
      ships[4] === 1 &&
      ships[5] === 0;

    return <div className={style.shipsTracker}>
      <div className={`${style.row} ${this.getStyles('one')}`}>
        <div className={style.ship}>
          <div className={style.cell}/>
        </div>
        <div className={style.ship}>
          <div className={style.cell}/>
        </div>
        <div className={style.ship}>
          <div className={style.cell}/>
        </div>
        <div className={style.ship}>
          <div className={style.cell}/>
        </div>
      </div>

      <div className={`${style.row} ${this.getStyles('two')}`}>
        <div className={style.ship}>
          <div className={style.cell}/>
          <div className={style.cell}/>
        </div>
        <div className={style.ship}>
          <div className={style.cell}/>
          <div className={style.cell}/>
        </div>
        <div className={style.ship}>
          <div className={style.cell}/>
          <div className={style.cell}/>
        </div>
      </div>

      <div className={`${style.row} ${this.getStyles('three')}`}>
        <div className={style.ship}>
          <div className={style.cell}/>
          <div className={style.cell}/>
          <div className={style.cell}/>
        </div>
        <div className={style.ship}>
          <div className={style.cell}/>
          <div className={style.cell}/>
          <div className={style.cell}/>
        </div>
      </div>

      <div className={`${style.row} ${this.getStyles('four')} ${style.center}`}>
        <div className={style.ship}>
          <div className={style.cell}/>
          <div className={style.cell}/>
          <div className={style.cell}/>
          <div className={style.cell}/>
        </div>
      </div>
      {/*<div>Кораблей размера 1 - {ships[1]} из 4.</div>*/}
      {/*<div>Кораблей размера 2 - {ships[2]} из 3.</div>*/}
      {/*<div>Кораблей размера 3 - {ships[3]} из 2.</div>*/}
      {/*<div>Кораблей размера 4 - {ships[4]} из 1.</div>*/}
      {isValid && <button onClick={this.handleStartGame}>Начать игру!</button>}
    </div>;
  }
}
