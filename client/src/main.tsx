import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import { createBrowserHistory } from 'history';
import { configureStore } from 'app/store';
import { App } from './app';

import socket from 'app/utils/socket';

// prepare store
const history = createBrowserHistory();
const store = configureStore(history);

socket.on('message', (message: any) => {
  console.log('socket says:', message);
  store.dispatch(message);
});

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
);
