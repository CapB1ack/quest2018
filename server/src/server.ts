import errorHandler from "errorhandler";
import * as http from "http";
import app from "./app";
import { Socket } from "socket.io";

const io = require("socket.io").listen(9999);
/**
 * Error Handler. Provides full stack - remove for production
 */
app.use(errorHandler());

/**
 * Start Express server.
 */
const server = http.createServer(app);
let queue: string[] = [];
const clientsById: {[key: string]: Socket} = {};
const connector: {[key: string]: string} = {};

export enum ESocketActions {
    INIT = "INIT", WAITING = "WAITING", START = "START", CONNECTED = "CONNECTED"
}

export interface ISocketMessage {
    type: ESocketActions;
    payload: object;
}

io.sockets.on("connection", (socket: Socket) => {
    const id = (socket.id).toString();
    const time = (new Date).toLocaleTimeString();
    // Посылаем клиенту сообщение о том, что он успешно подключился и его имя
    socket.json.send({
        type: ESocketActions.CONNECTED,
        payload: {
            id,
            time
        }
    });
    // Навешиваем обработчик на входящее сообщение
    socket.on("message", (msg: ISocketMessage) => {
        console.log("got message", msg, msg["type"]);
        if (msg.type === "INIT") {
            console.log("INIT", id);
            // Первый в очереди - кладем в саму очередь, клиенту отвечаем ожиданием.
            if (queue.length === 0) {
                console.log("queue.length === 0");
                socket.json.send({
                    type: ESocketActions.WAITING,
                    payload: {}
                });
                clientsById[id] = socket;
                queue.push(id);

                // В очереди уже есть клиент - связываем их, очищаем очередь
            } else if (queue.length === 1) {
                console.log("queue.length === 1");
                const waitingClient = queue.pop();
                connector[waitingClient] = id;
                connector[id] = waitingClient;

                // найдена пара игроков, оповещаем и первому даем право хода.
                clientsById[waitingClient].json.send({
                    type: ESocketActions.START,
                    payload: {isPlayerTurn: true}
                });
                socket.json.send({
                    type: ESocketActions.START,
                    payload: {isPlayerTurn: false}
                });
            }
        }
    });

    // При отключении клиента - уведомляем остальных
    socket.on("disconnect", () => {
        delete clientsById[id];
        queue = queue.filter(q => q !== id);
    });
});

// initialize the WebSocket server instance
// const wss = new WebSocket.Server({ server });

//

/*
wss.on("connection", (ws: WebSocket) => {
    console.log("new connection", wss.clients.size);
    // connection is up, let's add a simple simple event
    ws.on("message", (rawMessage: string) => {
        const message: ISocketMessage = JSON.parse(rawMessage);
        const {id} = message;
        console.log("got msg", message);
        // TODO: проверять на дохлые сокеты
        switch (message.action) {
            case ESocketActions.INIT:
                console.log("INIT", message.id);

                // Первый в очереди - кладем в саму очередь, клиенту отвечаем ожиданием.
                if (queue.length === 0) {
                    ws.send(JSON.stringify({
                        action: ESocketActions.WAITING
                    }));
                    clientsById[id] = ws;
                    queue.push(id);

                // В очереди уже есть клиент - связываем их, очищаем очередь
                } else if (queue.length === 1) {
                    const waitingClient = queue.pop();
                    connector[waitingClient] = id;
                    connector[id] = waitingClient;

                    // найдена пара игроков, оповещаем и первому даем право хода.
                    clientsById[waitingClient].send(JSON.stringify({
                        action: ESocketActions.START,
                        payload: true
                    }));

                    ws.send(JSON.stringify({
                        action: ESocketActions.START,
                        payload: false
                    }));
                }
        }

        // log the received message and send it back to the client
        // console.log("received msg: %s", message);

        // const broadcastRegex = /^broadcast\:/;
        //
        // if (broadcastRegex.test(message)) {
        //    message = message.replace(broadcastRegex, "");
        //
        //    // send back the message to the other clients
        //    wss.clients
        //    .forEach(client => {
        //        if (client != ws) {
        //            client.send(JSON.stringify({cellNumber: "2-1"}));
        //        }
        //    });
        //
        // } else {
        //    ws.send(`Hello, you sent -> ${message}`);
        // }
    });
});

*/
server.listen(app.get("port"), () => {
    console.log("  App is running at http://localhost:%d in %s mode", app.get("port"), app.get("env"));
    console.log("  Press CTRL-C to stop\n");
});

export default server;
