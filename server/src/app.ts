import express, { Application } from "express";
import compression from "compression";
import bodyParser from "body-parser";
import logger from "./util/logger";
import lusca from "lusca";
import dotenv from "dotenv";
import flash from "express-flash";
import path from "path";
import passport from "passport";
import expressValidator from "express-validator";
import { userSessionMiddleware } from "./db/db";


// Controllers (route handlers)
import * as homeController from "./controllers/home";
import * as addController from "./controllers/addOrder";

// Load environment variables from .env file, where API keys and passwords are configured
dotenv.config({path: ".env"});

// Create Express server
const app: Application = express();

// Express configuration
app.set("port", process.env.PORT || 9900);
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(expressValidator());
app.use(userSessionMiddleware);
// app.use(passport.initialize());
// app.use(passport.session());
app.use(flash());
app.use(lusca.xframe("SAMEORIGIN"));
app.use(lusca.xssProtection(true));
app.use((req, res, next) => {
    res.locals.user = req.user;
    next();
});
app.use(
    express.static(path.join(__dirname, "public"), {maxAge: 31557600000})
);

app.get("/", homeController.index);
app.get("/addOrder", addController.index);

export default app;