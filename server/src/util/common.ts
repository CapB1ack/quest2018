export function extendDefaultFields(defaults: any, session: any) {
    return {
        data: defaults.data,
        expires: defaults.expires,
        userId: session.userId
    };
}