import Sequelize from "sequelize";
import connectSes from "connect-session-sequelize";
import session from "express-session";
import { SESSION_SECRET } from "../util/secrets";
import { extendDefaultFields } from "../util/common";

export const sequelize = new Sequelize("postgres://db_user:123@localhost:5432/CRM");
export const SequelizeStore = connectSes(session.Store);

sequelize
    .authenticate()
    .then(() => {
        console.log("Connection has been established successfully.");
    })
    .catch(err => {
        console.error("Unable to connect to the database:", err);
    });

sequelize.define("user_session", {
    sid: {
        type: Sequelize.STRING,
        primaryKey: true
    },
    userId: Sequelize.STRING,
    expires: Sequelize.DATE,
    data: Sequelize.STRING(50000)
});

sequelize.query("LISTEN addedOrder");


const store = new SequelizeStore({
    db: sequelize,
    table: "user_session",
    extendDefaultFields
});
store.sync();

export const userSessionMiddleware = session({
    resave: true,
    saveUninitialized: true,
    secret: SESSION_SECRET,
    cookie: {maxAge: 30 * 24 * 60 * 60 * 1000},
    store
});