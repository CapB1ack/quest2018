import { INTEGER, or, STRING, UUID, UUIDV1 } from "sequelize";
import { sequelize } from "../db/db";


export const Order = sequelize.define("Order", {
    id: {
        type: UUID,
        defaultValue: UUIDV1,
        primaryKey: true
    },
    description: {
        type: STRING
    },
    count: {
        type: INTEGER
    },
    ownwer: {
        type: STRING
    }
}, {
    hooks: {
        afterCreate: (order: any, opt: any) => {
            console.log("hoook afterCreate", order, opt);
        }
    }
});

