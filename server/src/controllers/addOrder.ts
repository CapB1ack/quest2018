import { Request, Response } from "express";
import { Order } from "../models/order";

/**
 * GET /
 * Home page.
 */
export let index = async (req: Request, res: Response) => {
    await Order.sync();
    await Order.create({
        id: void 0,
        description: "test elem",
        count: 1,
        ownwer: "vozpp"
    });
    res.send("Added");
};
