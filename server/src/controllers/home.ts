import { Request, Response } from "express";
import { Order } from "../models/order";

/**
 * GET /
 * Home page.
 */
export let index = async (req: Request, res: Response) => {
    await Order.sync();
    const orders = await Order.findAll();
    res.send(orders);
};
